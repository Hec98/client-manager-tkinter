from tkinter import Tk
from tkinter.ttk import Treeview

root = Tk()
root.title('Client manager - tkinter')

tree = Treeview(root)
tree['columns'] = ('Name', 'Telephone', 'Company')

#tree.column('#0', width=80, anchor='center')
tree.column('#0', width=0, stretch='NO')
tree.column('Name', width=80, anchor='center')
tree.column('Telephone', width=80, anchor='center')
tree.column('Company', width=80, anchor='center')

#tree.heading('#0', text='id')
tree.heading('#0')
tree.heading('Name', text='Name')
tree.heading('Telephone', text='Telephone')
tree.heading('Company', text='Company')


tree.insert('', 'end', 'lala', values=('1', '2', '3'), text='Lili')
tree.insert('', 'end', 'lula', values=('1', '2', '3'), text='Lali')
#tree.insert('lala', 'end', 'lila', values=('1', '2', '3'), text='Luli')
tree.insert('', 'end', 'lila', values=('1', '2', '3'), text='Luli')

tree.grid(row=0, column=0)

root.mainloop()
