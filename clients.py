from tkinter import Tk, Button, Toplevel, Label, Entry, messagebox
from tkinter.ttk import Treeview
import sqlite3

root = Tk()
root.title('Client manager - tkinter')

conn = sqlite3.connect('data.db')
c = conn.cursor()

c.execute("""
          CREATE TABLE IF NOT EXISTS client (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            telephone TEXT NOT NULL,
            company TEXT NOT NULL
          );
""")
def new_client():
    def insert(client): print(client)
    def save():
        if not name.get or not telephone.get() or not company.get(): 
            messagebox.showerror('Error', 'Data not define')
            return
        client = {
            'name': name.get(),
            'telephone': telephone.get(),
            'company': company.get()
        }
        insert(client)
        top.destroy()
        
    top = Toplevel()
    top.title('Add client')

    lb_name = Label(top, text='Name: ')
    lb_name.grid(row=0, column=0)
    name = Entry(top, width=40)
    name.grid(row=0, column=1)

    lb_telephone = Label(top, text='Telephone: ')
    lb_telephone.grid(row=1, column=0)
    telephone = Entry(top, width=40)
    telephone.grid(row=1, column=1)

    lb_company = Label(top, text='Company: ')
    lb_company.grid(row=2, column=0)
    company = Entry(top, width=40)
    company.grid(row=2, column=1)
    
    bnt_save = Button(top, text='Save', command=save)
    bnt_save.grid(row=3, column=1)

    top.mainloop()


def delete_client(): pass

btn = Button(root, text='New client', command=new_client)
btn.grid(row=0, column=0)

btn_delete = Button(root, text='Delete client', command=delete_client)
btn_delete.grid(row=0, column=1)

tree = Treeview(root)
tree['columns'] = ('Name', 'Telephone', 'Company')

#tree.column('#0', width=80, anchor='center')
tree.column('#0', width=0, stretch='NO')
tree.column('Name', width=100, anchor='center')
tree.column('Telephone', width=100, anchor='center')
tree.column('Company', width=100, anchor='center')

#tree.heading('#0', text='id')
tree.heading('#0')
tree.heading('Name', text='Name')
tree.heading('Telephone', text='Telephone')
tree.heading('Company', text='Company')



tree.grid(row=1, column=0, columnspan=2)

root.mainloop()

